package fi.janho.varas.nfc.listening

abstract class Command {
    companion object {
        const val success = 0x20.toByte()
        const val invalidParameters = 0x44.toByte()
    }

    abstract fun start(state: ApduServiceState): ApduServiceState

    abstract fun processCommandApdu(commandApdu: ByteArray, state: ApduServiceState):
            Pair<ByteArray, ApduServiceState>

    abstract fun end(state: ApduServiceState): ApduServiceState
}
