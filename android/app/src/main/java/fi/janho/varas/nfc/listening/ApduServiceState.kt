package fi.janho.varas.nfc.listening

data class ApduServiceState(val status: String, val transactionAmount: Int? = null) {

    override fun toString(): String {
        return "$status, $transactionAmount"
    }
}
