package fi.janho.varas.storage

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class Transaction(
        @ColumnInfo(name = "type") var type: String,
        @ColumnInfo(name = "amount") var amount: Int,
        @ColumnInfo(name = "amount_after") var amountAfter: Int,
        @ColumnInfo(name = "initiator") var initiator: String,
        @ColumnInfo(name = "recipient") var recipient: String
) {
    @PrimaryKey(autoGenerate = true)
    var uid: Int = 0

    @ColumnInfo(name = "timestamp")
    var timestamp: String = (System.currentTimeMillis()/1000).toString()
}
