package fi.janho.varas.nfc

class Utils {
    companion object {
        fun bytesToHex(bytes: ByteArray): String {
            var s = ""
            for (b in bytes) {
                s += String.format("%02X", b)
            }
            return s
        }
    }
}
