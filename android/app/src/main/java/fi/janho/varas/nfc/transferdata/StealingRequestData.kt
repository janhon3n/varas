package fi.janho.varas.nfc.transferdata

import java.nio.ByteBuffer

class StealingRequestData {
    var speed: Int = 1

    constructor(array: ByteArray) {
        val buffer = ByteBuffer.wrap(array)
        this.speed = buffer.getInt()
    }

    constructor(speed: Int) {
        this.speed = speed
    }

    fun toByteArray(): ByteArray {
        val buffer = ByteBuffer.allocate(2+4)
        buffer.putInt(speed)
        return buffer.array()
    }
}