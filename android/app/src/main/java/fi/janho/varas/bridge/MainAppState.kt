package fi.janho.varas.bridge

import com.facebook.react.bridge.Arguments
import com.facebook.react.bridge.WritableMap

data class MainAppState(
        val walletId: String,
        var money: Int = 0,
        var apduListenerStatus: String = "Waiting",
        var apduTransactionAmount: Int = 0,
        var stealingModeEnabled: Boolean = false
) {

    fun toReactMap(): WritableMap {
        val map = Arguments.createMap()
        map.putString("walletId", walletId)
        map.putInt("money", money)
        map.putString("apduListenerStatus", apduListenerStatus)
        map.putInt("apduTransactionAmount", apduTransactionAmount)
        map.putBoolean("stealingModeEnabled", stealingModeEnabled)
        return map
    }

    override fun toString(): String {
        return "MainAppState: walletId=$walletId, money=$money, apduListenerStatus=$apduListenerStatus, apduTransactionAmount=$apduTransactionAmount, stealingModeEnabled=$stealingModeEnabled"
    }
}
