package fi.janho.varas.storage

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query

@Dao
interface TransactionDao {
    @Query("SELECT * FROM `transaction` ORDER BY timestamp DESC LIMIT 1")
    fun getLatest(): Transaction

    @Insert
    fun insert(transaction: Transaction)
}
