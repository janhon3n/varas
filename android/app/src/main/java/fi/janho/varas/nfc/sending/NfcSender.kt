package fi.janho.varas.nfc.sending

import android.content.Context
import android.nfc.NfcAdapter
import android.nfc.Tag
import android.nfc.tech.IsoDep
import android.os.Handler
import android.util.Log


class NfcSender(private val context: Context) : NfcAdapter.ReaderCallback {

    var stealingSender: StealingSender? = null

    var isoDep: IsoDep? = null
    var handler = Handler()
    val tickDelay: Long = 1000 //milliseconds

    private val selectAidApdu: ByteArray =
            intArrayOf(0x00, 0xA4, 0x04, 0x00, 0x07, 0xF0, 0x00, 0x71, 0x82, 0x73, 0x81, 0x23)
                    .map { it.toByte() }.toByteArray()

    private val tick : Runnable = object : Runnable {
        override fun run() {
            try {
                val requestsBytes = stealingSender!!.getRequest()
                val responseBytes = isoDep!!.transceive(requestsBytes)
                stealingSender!!.handleResponse(responseBytes)
                handler.postDelayed(this, tickDelay)
            } catch (ex: Exception) {
                Log.w(this.javaClass.name, "Exception: $ex")
                endConnection()
            }
        }
    }

    override fun onTagDiscovered(tag: Tag?) {
        Log.i(this.javaClass.name, "New tag discovered")
        isoDep = IsoDep.get(tag)
        Log.i(this.javaClass.name, "IsoDep: $isoDep")
        try {
            isoDep!!.connect()
            stealingSender = StealingSender(context)
            Log.i(this.javaClass.name, "New connection")
            val response = isoDep!!.transceive(selectAidApdu)
            Log.i(this.javaClass.name, response.joinToString { "%02x".format(it) })
            if (response[0] != 0x24.toByte()) throw Exception("Select aid initialization failed")
            tick.run()
        } catch(ex: Exception) {
            Log.w(this.javaClass.name, "Exception: $ex")
            endConnection()
        }
    }

    fun endConnection() {
        isoDep = null
        Log.i(this.javaClass.name, "Connection over")
        stealingSender?.destroy()
        stealingSender = null
    }

    fun cleanup() {}
}
