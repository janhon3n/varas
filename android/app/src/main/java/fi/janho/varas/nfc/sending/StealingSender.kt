package fi.janho.varas.nfc.sending;

import android.content.Context
import android.util.Log
import fi.janho.varas.nfc.transferdata.StealingRequestData
import fi.janho.varas.nfc.transferdata.StealingResponseData

class StealingSender(private val context: Context) {

    private var totalAmount = 0

    fun getRequest(): ByteArray {
        val request = StealingRequestData(5)
        return byteArrayOf(0x11, *request.toByteArray())
    }

    fun handleResponse(responseBytes: ByteArray) {
        if (responseBytes[0] == 0x44.toByte()) throw Exception("Response marked as error")
        val response = StealingResponseData(responseBytes)
        totalAmount += response.amount
        broadcastStealing(response.amount, "TEST ATTACKER")
        Log.i(this.javaClass.name, "Response: $response")
    }

    fun destroy() {
        broadcastStealing(totalAmount, "TEST ATTACKER", true)
    }

    private fun broadcastStealing(amount: Int, attacker: String, finished: Boolean) {
    }

    private fun broadcastStealing(amount: Int, attacker: String) {
        broadcastStealing(amount, attacker)
    }
}
