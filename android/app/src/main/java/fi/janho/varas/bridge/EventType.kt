package fi.janho.varas.bridge

enum class EventType {
    STEALING_MONEY, LOSING_MONEY
}