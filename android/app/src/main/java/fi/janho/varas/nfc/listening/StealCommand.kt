package fi.janho.varas.nfc.listening

import android.util.Log
import fi.janho.varas.nfc.Utils
import fi.janho.varas.storage.AppDatabase
import fi.janho.varas.storage.Transaction

class StealCommand(private val recipient: String) : Command() {

    companion object {
        const val firstApduByte = 0x01.toByte()
    }

    private var amountStolen = 0
    private var initiator: String? = null


    override fun start(state: ApduServiceState): ApduServiceState {
        return ApduServiceState("UnderSteal", 0)
    }

    override fun processCommandApdu(commandApdu: ByteArray, state: ApduServiceState):
            Pair<ByteArray, ApduServiceState> {

        if (commandApdu.size != 1 + 1 + 16)
            return Pair(byteArrayOf(invalidParameters), state)

        val power = commandApdu[1]
        val walletId = commandApdu.copyOfRange(2, 2 + 16)

        Log.i(this.javaClass.name, "Received steal apdu: $power $walletId")

        amountStolen += 10
        initiator = Utils.bytesToHex(walletId)

        return Pair(
                byteArrayOf(success),
                ApduServiceState("UnderSteal", amountStolen)
        )
    }

    override fun end(state: ApduServiceState): ApduServiceState {
        return ApduServiceState("Waiting")
    }
}
