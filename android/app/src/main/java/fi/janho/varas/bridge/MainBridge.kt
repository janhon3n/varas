package fi.janho.varas.bridge

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.nfc.NfcAdapter
import android.util.Log
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import androidx.room.Room
import com.facebook.react.bridge.LifecycleEventListener
import com.facebook.react.bridge.ReactApplicationContext
import com.facebook.react.bridge.ReactContextBaseJavaModule
import com.facebook.react.bridge.ReactMethod
import com.facebook.react.modules.core.DeviceEventManagerModule.RCTDeviceEventEmitter
import fi.janho.varas.nfc.listening.ApduListeningService
import fi.janho.varas.nfc.sending.NfcSender
import fi.janho.varas.storage.AppDatabase
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch


class MainBridge(reactContext: ReactApplicationContext) : ReactContextBaseJavaModule(reactContext), LifecycleEventListener {

    init {
        reactContext.addLifecycleEventListener(this);
    }

    override fun getName(): String = "MainBridge"
    override fun getConstants(): MutableMap<String, Any> = mutableMapOf()

    val db = Room.databaseBuilder(
            reactContext,
            AppDatabase::class.java, "database-name"
    ).build()

    val state = MainAppState("testi", 5000)

    var nfcSender: NfcSender? = null

    @ReactMethod
    fun requestState() {
        sendState()
    }

    @ReactMethod
    fun setStealingMode(enabled: Boolean) {
        when (enabled) {
            true -> {
                if (nfcSender!= null){
                    nfcSender!!.cleanup()
                    nfcSender = null
                }
                nfcSender = NfcSender(reactApplicationContext)

                NfcAdapter.getDefaultAdapter(reactApplicationContext).enableReaderMode(
                        reactApplicationContext.currentActivity,nfcSender, NfcAdapter.FLAG_READER_NFC_A, null)

                state.stealingModeEnabled = true
                sendState()
            }
            false -> {
                if (nfcSender!= null){
                    nfcSender!!.cleanup()
                    nfcSender = null
                }

                NfcAdapter.getDefaultAdapter(reactApplicationContext).disableReaderMode(
                        reactApplicationContext.currentActivity
                )

                state.stealingModeEnabled = false
                sendState()
            }
        }
        Log.i(this.javaClass.name, "Stealing mode: $enabled")
    }

    @ReactMethod
    fun startApduListeningService() {
        Log.i(this.javaClass.name, "Starting apdu listening service")
        val serviceIntent = Intent(reactApplicationContext, ApduListeningService::class.java)
        reactApplicationContext.startService(serviceIntent)
    }

    private fun sendState() {
        Log.i(this.javaClass.name, "Sending stateChanged event: ${state.toString()}")
        reactApplicationContext.getJSModule(RCTDeviceEventEmitter::class.java)
                .emit("stateChanged", state.toReactMap())
    }

    override fun onHostResume() {
        val receiver = object : BroadcastReceiver() {
            override fun onReceive(context: Context?, intent: Intent?) {
                if (intent == null) return
                Log.i(this.javaClass.name, "Received broadcast from ApduListeningService")
                state.apduListenerStatus = intent.getStringExtra("status") ?: "Waiting"
                state.apduTransactionAmount = intent.getIntExtra("transactionAmount", 0)
                sendState()
            }
        }

        LocalBroadcastManager.getInstance(reactApplicationContext)
                .registerReceiver(
                        receiver,
                        IntentFilter("apdu-listening-service-state-changed")
                );
    }

    override fun onHostPause() {
    }

    override fun onHostDestroy() {
    }
}
