package fi.janho.varas.nfc.transferdata

import java.nio.ByteBuffer

class StealingResponseData {
    val amount: Int

    constructor(array: ByteArray) {
        val buffer = ByteBuffer.wrap(array)
        amount = buffer.getInt()
    }

    constructor(amount: Int) {
        this.amount = amount
    }

    fun toByteArray(): ByteArray {
        val buffer = ByteBuffer.allocate(2+4)
        buffer.putInt(amount)
        return buffer.array()
    }
}