package fi.janho.varas.nfc.listening

import android.content.Intent
import android.nfc.cardemulation.HostApduService
import android.os.Bundle
import android.util.Log
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import androidx.room.Room
import fi.janho.varas.storage.AppDatabase


class ApduListeningService : HostApduService() {

    private val commandNotFound = 0x44.toByte()
    private val initializationOk = 0x24.toByte()

    private val selectAidApdu: ByteArray =
            intArrayOf(0x00, 0xA4, 0x04, 0x00, 0x07, 0xF0, 0x00, 0x71, 0x82, 0x73, 0x81, 0x23)
                    .map { it.toByte() }.toByteArray()

    private lateinit var state: ApduServiceState

    private fun updateState(newState: ApduServiceState) {
        state = newState
        broadcastState()
    }

    override fun onCreate() {
        Log.i(this.javaClass.name, "ApduListeningService created")
        updateState(ApduServiceState("Waiting"))
    }

    var command: Command? = null
    override fun processCommandApdu(commandApdu: ByteArray, extras: Bundle?): ByteArray {
        Log.i(this.javaClass.name, "Incoming apdu: $commandApdu")

        if (commandApdu.contentEquals(selectAidApdu)) {
            initializeNewConnection()
            return byteArrayOf(initializationOk)
        }

        if (commandApdu[0] == StealCommand.firstApduByte) {
            if (command == null) {
                command = StealCommand("testi")
                state = command!!.start(state)
            }
            val res = command!!.processCommandApdu(commandApdu, state)
            updateState(res.second)
            return res.first
        }

        return byteArrayOf(commandNotFound)
    }

    override fun onDeactivated(reason: Int) {
        if (command != null) {
            updateState(command!!.end(state))
        }
    }

    private fun initializeNewConnection() {
    }

    private fun broadcastState() {
        val intent = Intent("apdu-listening-service-state-changed")
        intent.putExtra("status", state.status)
        intent.putExtra("transactionAmount", state.transactionAmount)
        LocalBroadcastManager.getInstance(this).sendBroadcast(intent)
    }
}
