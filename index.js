/**
 * @format
 */

import {AppRegistry } from 'react-native'
import Main from './source/Main'
import {name as appName} from './app.json'


AppRegistry.registerComponent('Varas', () => Main)
