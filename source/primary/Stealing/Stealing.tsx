import React, { useState, useEffect } from 'react'
import { StyleSheet, View } from 'react-native'
import useAnimatedAmount from '../../reusable/AnimatedAmount'
import { formatAmount } from '../../reusable/Utils'
import MyText from '../../reusable/MyText'
import { observer } from 'mobx-react-lite'
import { useGlobalState } from '../../state/GlobalStateContext'
import mainBridgeInterface from '../../state/MainBridge'

export default observer(() => {
    const state = useGlobalState()

    const [amount, setAmount] = useState(0)

    const animatedAmount = useAnimatedAmount(amount)

    useEffect(() => {
        mainBridgeInterface.setStealingMode(true)
        return () => {
            mainBridgeInterface.setStealingMode(false)
        }
    }, [])

    if (!state.stealingModeEnabled) return (
        <View style={styles.container}>
            <MyText>...</MyText>
        </View>
    )

    return (
        <View style={styles.container}>
            <MyText style={{ fontSize: 20 }}>Ready to steal</MyText>
            <MyText style={{ fontSize: 40 }}>{formatAmount(animatedAmount)}</MyText>
        </View>
    )
})


const styles = StyleSheet.create({
    container: { flex: 1, justifyContent: 'center', alignItems: 'center' },
})
