import React, { useState, useEffect } from 'react'
import { View, Text } from 'react-native'

interface Entry {
    time: number;
    text: string;
}

export default function Log() {
    const [logs, setLogs] = useState<Entry[]>([])

    return (
        <View style={{ backgroundColor: '#111', padding: 5 }}>
            {logs.slice(logs.length - 3).map((entry: Entry, i: number) => (
                <Text
                    key={i}
                    style={{
                        fontFamily: 'monospace',
                        color: '#AAA',
                        padding: 5,
                    }}
                >
                    {entry.time + ': ' + entry.text}
                </Text>
            ))}
        </View>
    )
}
