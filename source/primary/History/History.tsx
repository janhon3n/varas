import React from 'react'
import { View, StyleProp, ViewStyle, StyleSheet } from 'react-native'

interface Props {
    style?: StyleProp<ViewStyle>;
}

export function History(props: Props) {
    const { style } = props

    return (
        <View style={[style]}>

        </View>
    )
}

const styles = StyleSheet.create({

})
