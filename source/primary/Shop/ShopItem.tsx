import React, { useState } from 'react'
import { View, TouchableWithoutFeedback } from 'react-native'
import Collapsible from 'react-native-collapsible'
import MyText from '../../reusable/MyText'
import RoundButton from '../../reusable/RoundButton'
import { formatAmount } from '../../reusable/Utils'
import { colors } from '../../Theme'

interface ShopItem {
    name: string;
    price: number;
    description: string;
}

interface Props {
    shopItem: ShopItem;
}

export default function ShopItem(props: Props) {
    const [collapsed, setCollapsed] = useState(false)
    return (
        <TouchableWithoutFeedback
            onPress={() => {
                console.log(collapsed)
                setCollapsed(!collapsed)
            }}
        >
            <View
                style={{
                    padding: 10,
                    flexDirection: 'column',
                    borderColor: '#151515',
                    borderBottomWidth: 4,
                }}
            >
                <View
                    style={{
                        flexDirection: 'row',
                        alignItems: 'center',
                    }}
                >
                    <MyText
                        style={{ flex: 1, fontSize: 18, color: colors.primary }}
                    >
                        {props.shopItem.name}
                    </MyText>
                    <MyText style={{ fontSize: 16 }}>
                        {formatAmount(props.shopItem.price)}
                    </MyText>
                </View>

                <Collapsible collapsed={collapsed}>
                    <View
                        style={{
                            marginTop: 10,
                            flexDirection: 'row',
                            alignItems: 'center',
                        }}
                    >
                        <MyText style={{ flex: 1, fontSize: 14 }}>
                            {props.shopItem.description}
                        </MyText>
                        <RoundButton
                            title="Buy"
                            onPress={() => { }}
                            size="tiny"
                        />
                    </View>
                </Collapsible>
            </View>
        </TouchableWithoutFeedback>
    )
}
