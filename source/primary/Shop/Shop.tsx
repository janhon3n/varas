import React from 'react'
import { ScrollView } from 'react-native'
import ShopItem from './ShopItem'

const shopItems = [
    {
        id: 1,
        name: 'Name scrambler',
        price: 100,
        description: 'Steal without leaving your name into the victims record',
    },
    {
        id: 2,
        name: 'Alarm system',
        price: 150,
        description: 'Rign an alarm when somebody is trying to steal from you',
    },
    {
        id: 3,
        name: 'NVidia Titan X',
        price: 4000,
        description: 'Boost stealing performance by 100%',
    },
    {
        id: 4,
        name: 'Name scrambler',
        price: 100,
        description:
            'Steal without leaving your name into the victims record. Steal without leaving your name into the victims record. Steal without leaving your name into the victims record. ',
    },
    {
        id: 5,
        name: 'Alarm system',
        price: 150,
        description: 'Rign an alarm when somebody is trying to steal from you',
    },
    {
        id: 6,
        name: 'NVidia Titan X',
        price: 4000,
        description: 'Boost stealing performance by 100%',
    },
]

export default function Shop() {
    return (
        <ScrollView style={{ backgroundColor: '#111', flex: 1 }}>
            {shopItems.map(item => (
                <ShopItem key={item.id} shopItem={item} />
            ))}
        </ScrollView>
    )
}
