import React from 'react'
import MyText from '../../reusable/MyText'
import { formatAmount } from '../../reusable/Utils'
import { useGlobalState } from '../../state/GlobalStateContext'
import { observer } from 'mobx-react-lite'
import { StyleProp, ViewStyle } from 'react-native'

interface Props {
    style?: StyleProp<ViewStyle>;
}

export default observer((props: Props) => {
    const { style } = props

    const state = useGlobalState()

    return (
        <MyText
            style={[
                {
                    textAlign: 'center',
                    fontSize: 50,
                    fontWeight: 'bold',
                },
                style,
            ]}
        >
            {formatAmount(state.money)}
        </MyText>
    )
})
