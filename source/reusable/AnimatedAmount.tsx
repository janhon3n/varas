import { useEffect, useState } from 'react'

const rate = 15

export default function useAnimatedAmount(amount: number) {
    const [prevAmount, setPrevAmount] = useState(amount)
    const [iteration, setIteration] = useState(0)

    const [animatedAmount, setAnimatedAmount] = useState(amount)

    useEffect(() => {
        setPrevAmount(animatedAmount)
        setIteration(0)
    }, [amount])

    useEffect(() => {
        let timeout: NodeJS.Timeout
        if (iteration <= rate) {
            timeout = setTimeout(() => {
                setAnimatedAmount(
                    prevAmount + ((amount - prevAmount) * iteration) / rate
                )
                setIteration(iteration + 1)
            }, 1000 / rate)
        }
        return () => {
            if (timeout) clearTimeout(timeout)
        }
    }, [animatedAmount, prevAmount, amount, iteration])

    return animatedAmount
}
