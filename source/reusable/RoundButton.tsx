import React, { useState } from 'react'
import { View, TouchableWithoutFeedback } from 'react-native'
import MyText from './MyText'
import { colors } from '../Theme'

interface Props {
    title: string;
    onPress(): void;
    size?: null | 'small' | 'tiny';
}

export default function RoundButton(props: Props) {
    const [touched, setTouched] = useState(false)

    function getSize() {
        return props.size === 'tiny' ? 50 : props.size === 'small' ? 70 : 90
    }

    function getBorderSize() {
        return props.size === 'tiny' ? 2 : props.size === 'small' ? 3 : 3
    }

    return (
        <View
            style={{
                backgroundColor: touched ? colors.primary : '#AAA',
                borderRadius: 1000,
                justifyContent: 'center',
                alignItems: 'center',
                margin: 5,
            }}
        >
            <TouchableWithoutFeedback
                onPressIn={() => setTouched(true)}
                onPressOut={() => setTouched(false)}
                onPress={props.onPress}
            >
                <View
                    style={{
                        height: getSize(),
                        width: getSize(),
                        backgroundColor: '#666',
                        borderRadius: 1000,
                        justifyContent: 'center',
                        alignItems: 'center',
                        margin: getBorderSize(),
                    }}
                >
                    <MyText
                        style={{
                            color: 'black',
                            fontWeight: 'bold',
                            fontSize: 14,
                        }}
                    >
                        {props.title}
                    </MyText>
                </View>
            </TouchableWithoutFeedback>
        </View>
    )
}
