export function formatAmount(amount: number) {
    let euros: number = Math.floor(amount / 100)
    let cents: number = Math.round(amount) % 100
    let centsString = cents + ''
    centsString = '0'.repeat(2 - centsString.length) + centsString

    return euros + '.' + centsString + ' €'
}
