import React from 'react'
import { StyleProp, Text, TextStyle } from 'react-native'

interface Props {
    [key: string]: any;
    style?: StyleProp<TextStyle>;
}

export default function MyText(props: Props) {
    const style = {
        color: '#AAA',
        fontFamily: 'sans-serif-medium',
    }
    return <Text {...props} style={[style, props.style]} />
}
