import { makeAutoObservable } from 'mobx'

export default class GlobalState {
    constructor(
        public walletId: string,
        public money: number = 0,
        public apduListenerStatus: string = 'Waiting',
        public apduTransactionAmount: number = 0,
        public stealingModeEnabled: boolean = false
    ) {
        makeAutoObservable(this)
    }
}
