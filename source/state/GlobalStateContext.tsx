import { runInAction } from 'mobx'
import React, { ReactNode, useContext, useEffect } from 'react'
import { NativeEventEmitter, NativeModules } from 'react-native'
import GlobalState from './GlobalState'

const MainBridge = NativeModules.MainBridge

const globalState = new GlobalState('')

//@ts-ignore
const GlobalStateContext = React.createContext<GlobalState>(null)

interface Props {
    children: ReactNode;
}

export function GlobalStateProvider({ children }: Props) {

    // Listen to events from MainBridge and update state accordingly
    useEffect(() => {
        const eventEmitter = new NativeEventEmitter(MainBridge)
        const eventListener = eventEmitter.addListener('stateChanged', event => {
            console.info('stateChanged', event)
            runInAction(() => {
                globalState.money = event.money
                globalState.walletId = event.walletId
                globalState.stealingModeEnabled = event.stealingModeEnabled
                globalState.apduListenerStatus = event.apduListenerStatus
                globalState.apduTransactionAmount = event.apduTransactionAmount
            })
        })
        return () => {
            eventListener.remove()
        }
    }, [])

    useEffect(() => {
        MainBridge.requestState()
    }, [])

    return (
        <GlobalStateContext.Provider value={globalState}>
            {children}
        </GlobalStateContext.Provider>
    )
}


export function useGlobalState() {
    return useContext(GlobalStateContext)

}
