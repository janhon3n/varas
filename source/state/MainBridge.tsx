import { NativeModules } from 'react-native'
const MainBridge = NativeModules.MainBridge

type Listener<T> = (data: T) => void

interface MainBridgeInterface {
    setStealingMode(enabled: boolean): void;
    requestState: () => void;
    startApduListeningService(): void;
}

const mainBridgeInterface: MainBridgeInterface = {
    setStealingMode: MainBridge.setStealingMode,
    requestState: MainBridge.requestState,
    startApduListeningService: MainBridge.startApduListeningService,
}

export default mainBridgeInterface
