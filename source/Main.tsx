import React, { ReactNode, useEffect, useState } from 'react'
import { View, StyleSheet } from 'react-native'
import Money from './primary/Money/Money'
import RoundButton from './reusable/RoundButton'
import Stealing from './primary/Stealing/Stealing'
import Shop from './primary/Shop/Shop'
import { GlobalStateProvider } from './state/GlobalStateContext'
import { History } from './primary/History/History'
import mainBridgeInterface from './state/MainBridge'



export default function Main() {
    const [mode, setMode] = useState<'shop' | 'steal' | 'history' | null>(null)

    let content: ReactNode | null = null
    if (mode === 'shop') content = <Shop />
    if (mode === 'steal') content = <Stealing />
    if (mode === 'history') content = <History />

    useEffect(() => {
        mainBridgeInterface.startApduListeningService()
    }, [])


    return (
        <GlobalStateProvider>
            <View
                style={{
                    flex: 1,
                    flexDirection: 'column',
                    justifyContent: 'space-between',
                    alignItems: 'stretch',
                    backgroundColor: '#222',
                }}
            >
                <Money style={{ margin: 20 }} />
                {content && <>
                    <View style={styles.mainContent}>
                        {content}
                    </View>
                    <View style={styles.buttonRow}>
                        <RoundButton title="Cancel" onPress={setMode.bind(null, null)} />
                    </View>
                </>}

                {content == null && <>
                    <View style={styles.bottom}>
                        <View style={styles.buttonRow}>
                            <RoundButton
                                title="History"
                                onPress={() => {
                                    setMode('history')
                                }}
                            />
                            <RoundButton
                                title="Shop"
                                onPress={() => {
                                    setMode('shop')
                                }}
                            />
                            <RoundButton
                                title="Steal"
                                onPress={() => {
                                    setMode('steal')
                                }}
                            />
                        </View>
                    </View>
                </>}
            </View>
        </GlobalStateProvider >
    )
}

const styles = StyleSheet.create({
    top: {
        flex: 1,
    },
    topSmall: {
        height: 120,
    },
    center: {
        padding: 20,
    },
    mainContent: {
        flex: 1,
    },
    bottom: {
        flex: 1,
        justifyContent: 'flex-end',
    },
    buttonRow: {
        flexDirection: 'row',
        justifyContent: 'flex-end',
        alignItems: 'flex-end',
        padding: 20,
    },
})
